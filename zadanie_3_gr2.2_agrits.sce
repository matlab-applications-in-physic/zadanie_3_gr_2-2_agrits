//Matlab applications in physics
//Author: Artur Kasza
//Technical Physics

//WARNING: Please make sure to launch this program from the same direction that 
//the data folder is located
//Also please make sure you run the file without echo

//Defining functions to be used in solutions

//Function providing FOP-ish filter behaviour - filtering out falsy elements for given predicate
function coll=filterColl(inpColl, predicate)
    coll = []
    for i=1:size(inpColl)(1)
        for j=1:size(inpColl)(2)
            if predicate(inpColl(i,j)) then
                coll(i, j) = inpColl(i, j)
            end
        end
    end
endfunction

//Function providing FOP-ish map behaviour - applying given function to every element of given collection and returning a result
function coll=map(inpColl, func)
    coll = []
    for i=1:size(inpColl)(1)
        for j=1:size(inpColl)(2)
            coll(i, j) = func(inpColl(i,j))
        end
    end
endfunction

//Convert YYYY-mm-dd date to datenum
//See: datenum
function num=datenumFromStr(str)
    num = datenum(strtod(strsplit(str, "-"))')
endfunction


function comment=compareMeans(x1, x2)
    T = tTest(x1, x2)
    cv = 2
    //TODO: criticalValue(degreesOfFreedom(x1, x2))
    if T < cv then
        comment = "Day and night mean values of PM10 are not different."
    elseif T >= cv & T<cv+1 then
        comment = "The Welch test does not determine if day and night mean values of PM10 are different."
    else
        comment = "Day and night mean values of PM10 are different."
    end
endfunction

//Welch t test
function tScore=tTest(x1, x2)
    nominator = abs(mean(x1)-mean(x2))
    denominator = sqrt(stdev(x1)^2/length(x1) + stdev(x2)^2/length(x2))
    tScore = nominator/denominator
endfunction

function bool=notZero(num)
    bool = num<>0
endfunction

//Calculating degress of freedom for two samples using approximation obtained with use of Welch-Satterthwaite equation
function dof=degreesOfFreedom(x1, x2)
    nominator = ((stdev(x1)^2/length(x1))+(stdev(x2)^2/length(x2)))^2
    denominator = (stdev(x1)^4/(length(x1)^2*(length(x1)-1)) + stdev(x2)^4/(length(x2)^2*(length(x2)-1)))
    dof = nominator/denominator
endfunction


//Return critical value for t test for given dof
//Source: https://www.medcalc.org/manual/t-distribution.php
//Please note: Using 0.02 significance level, since working with two samples, thus it's the best possible approximation for signifance level of 0.05/2 (Dividing alpha by 2 because of two independent samples)
function criticalValue=criticalValue(dof)
    criticalValues = csvRead("tScore.csv", ",")
    for k = 1:length(criticalValues)/2
        if(dof>criticalValues(k, 1) & dof<criticalValues(k+1, 1))
            criticalValue = criticalValues(k, 2)
            break
        end
    end
endfunction


function [inRange, outOfRange]=filterByRange(m, s, e)
    inRange = []
    outOfRange = []
    for k = 1:size(m)(1)
        if strtod(m(k, 1)) > s & strtod(m(k, 1)) < e then
            inRange($+1, 1) = strtod(m(k, 6))
        else
            outOfRange($+1, 1) = strtod(m(k, 6))
        end
    end
endfunction

function _date=extractDateFromFileName(fileName)
    [s, e, _date] = regexp(fileName, "/\d{4}-\d{2}-\d{2}/")
endfunction

//Extracts every hour in 'PM/AM' notation from given JSON
function hours=extractHoursFromJSON(filename)
    json = mgetl(filename, 1)
    [s, e, hours] = regexp(json, "/\d+:\d+:\d+ [AP]M/")
endfunction

//Collects data from external API and saves it to file data.json
function filename=getJSONData(_date)
    url = strcat(['https://api.sunrise-sunset.org/json?lat=50.29761&lng=18.67658&date=', _date])
    filename = getURL(url, "data.json")
endfunction

//Determines astronomical dawn and dusk hours for given date
//Astronomical values are better for our use, since we are conducting 
//physical research
function [dawn, dusk]=getDawnAndDuskForDate(_date)
    filename = getJSONData(_date)
    hours = extractHoursFromJSON(filename)
    dawn = hours(8)
    dusk = hours(9)
endfunction

//Checks if given string represents AM time
function b=isTimeAM(time)
    b = grep(time, "AM")==1
endfunction

//Extracts hour from date and changes it to numeral value in 24-hour notation
function hour=getHour24FromTime(time)
    [s, e, h] = regexp(time, '/\d+(?=:)/', "o")
    if isTimeAM(time) then
        hour = strtod(h)
    else
        hour = strtod(h)+12
    end
endfunction

function [day, night]=getAveragesForDayAndNight(hours, values, _date)
    [dawn, dusk] = getDawnAndDuskForDate(_date)
    
endfunction

files = listfiles("data") //List of filenames
contents = list() //List of files' contents

//Variables used in finding maximal PM10 value
maxPM10 = 0
maxIndex = []
maxFile = []
nightValues = []
dayValues = []


//Read CSV data, insert row into list, find maximum of PM10
for k = 1:size(files)(1)
    contents(k) = csvRead("data/"+files(k, 1), ";", [], "string", [",", "."])
    [currMax, currMaxIndex] = max(strtod(contents(k)(2:25, 6)))
    _date = extractDateFromFileName(files(k))
    disp(_date, "Proccessing data for day ")
    duskSum = 0
    dawnSum = 0
    
    [dawn, dusk] = getDawnAndDuskForDate(_date)
    
    dawnHour = getHour24FromTime(dawn)
    duskHour = getHour24FromTime(dusk)
    
    [in, out] = filterByRange(contents(k)(2:25, :), dawnHour, duskHour)
    
    nightValues(k, :) = out
    dayValues(k, :) = in
    
    //Check if current maximum measurement is maximum PM10 in whole data set
    disp(strcat([string(currMax), "µg/m3"]), "Maximum in set: ")
    if currMax > maxPM10 then
        maxPM10 = currMax
        maxIndex = [currMaxIndex]
        maxFile = [k]
     elseif currMax == maxPM10 then
        maxIndex($) = currMaxIndex
        maxFile($) = currMaxFile
     end
end

function printData(s)
    maxHour = contents(maxFile)(maxIndex+1, 1)
    maxDate = extractDateFromFileName(files(maxFile))
    
    
    comment = msprintf(compareMeans(dayValues, nightValues))
    maxStr = msprintf('Maximum PM10 concentration was %d µg/m3 and was measured during 1 hour before %s %s'       ,maxPM10, maxHour, maxDate)
    periodString = msprintf("Analyzed period was: %s to %s", startDate, endDate)
    sortedDatenum = gsort(map(map(files, extractDateFromFileName), datenumFromStr))
    startDate = strcat(string(filterColl(datevec(sortedDatenum($)), notZero)), "-")
    endDate = strcat(string(filterColl(datevec(sortedDatenum(1)), notZero)), "-")
    printf(maxStr)
    printf("\n")
    printf("Conducting Welch test for means:")
    printf("\n")
    printf(comment)
    printf("\n")
    printf(periodString)
    printf("\n")
    print("PMresults.dat", periodString, comment, maxStr)
endfunction

printData()





